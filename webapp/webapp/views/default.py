import logging
import jsonschema

from jsonschema import validate
from datetime import datetime

from pyramid.view import view_config
from pyramid.httpexceptions import HTTPBadRequest, HTTPAccepted, HTTPInternalServerError
from asyncworker.tasks import merge_s3_files, download_task_status



logger = logging.getLogger(__name__)

@view_config(route_name="initiate", renderer="json", request_method="POST")
def initiate(request):
    """
    The function should initiate the merging of files on S3 with names between
    the given dates. The merging is offloaded to the async executor service.

    According to the assignment the expected POST input is:
    {
        "start_date": "<date in ISO8601 format>",
        "end_date": "<date in ISO8601 format>"
    }
    For example:
    {
        "start_date": "2019-08-18",
        "end_date": "2019-08-25"
    }

    The return data is a download ID that the /download endpoint digests:
    {
        "download_id": "<id>"
    }
    For example:
    {
        "download_id": "b0952099-3536-4ea0-a613-98509f4087cd"
    }
    """
    error = validate_request(request.json_body)
    if error:
        raise error
    logger.info("Initiate called")
    task_result = merge_s3_files.delay(request.json_body["start_date"], request.json_body["end_date"])
    return {"download_id" : task_result.id}


def validate_request(request_data):
    """
    Validate initiate request.
    :param request_data: request data of initiate request
    :return: None or HTTPBadRequest if validation fails
    """
    form = {
        "type": "object",
        "properties": {
            "start_date": {"type": "string"},
            "end_date": {"type": "string"}
        },
    }

    try:
        validate(instance=request_data, schema=form)
        date_format = '%Y-%m-%d'
        start_date = datetime.strptime(request_data["start_date"], date_format)
        end_date = datetime.strptime(request_data["end_date"], date_format)
        if start_date > end_date :
            return HTTPBadRequest("state date can't be less that end_date")
    except jsonschema.exceptions.ValidationError as err:
        return HTTPBadRequest("Invaid Schema")
    except ValueError:
        return HTTPBadRequest("Invalid format Valid format is  YYYY-MM-DD.")



@view_config(route_name="download", renderer="json")
def download(request):
    """
        This function is called when a GET request is made to /download.
        This endpoint accepts the download ID as a URL parameter and returns
        s3 path for file download. If the merging is not done yet, the
        appropriate HTTP code 202 is returned.
        The return data is a file url where the file is present.
        {
           "file": <file_path>
        }
        or

    """
    download_id = request.matchdict["download_id"]
    logger.info("Download called")
    task_result = download_task_status(download_id)
    if task_result is None:
        raise HTTPBadRequest("No such downloadId. Send a valid Id.")

    error = task_status(task_result)
    if error:
        raise error
    logger.info("Task result is %s", task_result.get())

    return {"file": task_result.get()}


def task_status(task):
    """
    Find the task status for the given download task.
    :param task: downlaod task to check the status.
    :return: HTTP status for not sucess states and None for sucess.
    """
    if task.status in ["SUCCESS"]:
        return None
    if task.status in ["PENDING", "STARTED"] :
        return HTTPAccepted("Merging in Progress.")
    else  :
        return HTTPInternalServerError("Error with merging. Please contact dev  team.")

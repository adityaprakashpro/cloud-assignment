
### Build the project and Run

docker-compose build

docker-compose up

### Expected REST API specification

### /initiate - POST

Input model:

    {
        "start_date": "<Date, ISO8601 format>",
        "end_date": "<Date, ISO8601 format>"
    }

Output :

    {
        "download_id": "b0952099-3536-4ea0-a613-98509f4087cd"
    }


#### /download/<download ID>` - `GET`
Output :
    
    {
        "file": <file_singed_url>
    }


To change the properties change in the docker-compose.yml and myConfig,py

There is catch with the minio server. If the server is up using docker compose
then DOCKER_MINIO_HOST and LOCAL_MINIO_HOST wil be different. This DOCKER_MINIO_HOST
is used to create the S3 connections, while LOCAL_MINIO_HOST is used for the signed url
generation . This both properties will be same if minio server is host externally.


### Assumptions Made

1. Lines in the file will start with timestamp.
2. The logs files are assumed to be present in  `logs` bucket. 
   To change that location update myConfig.py LOG_BUCKET property.
3. For the given start_date till end_date no log files are found 
   the task will generate "No log data present." and if certain 
   dates have log files then result data will have only those days. 


### Desgin Overview

For a given date range for example "2020-05-19"  to "2020-05-21" we
break it into 2 task, merge_intra_day task and merge_inter_day_task.

In Parallel for each day in the given date range a `merge_intra_day` task is created.
This task will generate file in sorted order from the multiple same day files present.
For example 2020-05-19" has "2020-05-19_1" , "2020-05-19_2" etc will be generate
a combined 2020-05-19 file. The combined file is stored in results bucket 
and not in the logs bucket. 

Then all the combined single day files will be concatenated 
in single file sorted on date of the file in `merge_inter_day task`. And the combined single
day files are deleted.


All the process happens in the cloud. At no point is the file downloaded in the local
server for processing. Except there is a caveat that in S3 for multiple part upload of files
smaller than 5 mb is handled by downloading the file and then upload it S3_Concat is used for this.

To handle race condition all the files are suffixed with a timestamp.


### Improvements 

1. Pre-Computation of  merge_intra_day task results.
The idea was to stored the results of the merge_intra_day as for older days
need logs files will not be created. This is not implemented.
Created a cache decorator and is a work in progress.

2. Caching the results of same date range
For same date ranges older generated file can be given rather than created a new task.

3. Need to add unit test cases.


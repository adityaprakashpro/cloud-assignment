"""
This is still work in progress. The idea was to create
a cache wrapper store the task_ids for key created by
function arguments of the cache.
Not integrated  into the solution.
"""

import json
from functools import wraps
from redis import StrictRedis
from asyncworker.celery import app

import logging


logger = logging.getLogger(__name__)



redis = StrictRedis(host='127.0.0.1', port=6379)


def cached(func):
    """
    Decorator that caches the results of the function call.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        # Generate the cache key from the function's arguments.
        key_parts = [func.__name__] + list(args)
        key = '-'.join(key_parts)
        result = redis.get(key)

        if result is None or not task_status(result):
            # Run the function and cache the result for next time.
            value = func(*args, **kwargs).delay()
            redis.set(key, value.id)
        else:
            # Skip the function entirely and use the cached value instead.
            # value_json = result.decode('utf-8')
            logging.info(result)
            value = get_task(result)

        return value
    return wrapper


# Auxilary methods

def task_status(task_id):
    task = get_task(task_id)
    if task.status in ["PENDING", "STARTED", "SUCCESS"] :
        return True
    elif task.status in ["FAILURE", "RETRY"] :
        return False
    else:
        return False

def get_task(task_id):
    return app.AsyncResult(task_id)

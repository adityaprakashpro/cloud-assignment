
import botocore
import logging

from datetime import timedelta
from s3_concat import S3Concat

from . import myconfig

logger = logging.getLogger(__name__)

class S3Utils:
    """
    Utility file for various S3 operations.
    """
    def __init__(self, s3_client, s3_fs,
                 cache_result_bucket, result_bucket):
        self.s3_client = s3_client
        self.s3_fs = s3_fs
        self.cache_result_bucket = cache_result_bucket
        self.result_bucket = result_bucket



    def create_bucket_if_required(self, bucket_name):
        """
        Create a bucket in s3 if doesn't exist.
        :param bucket_name: Name of the bucket
        """
        kwargs = {'Bucket': bucket_name}
        try:
            self.s3_client.head_bucket(**kwargs)
        except botocore.exceptions.ClientError as e:
            error_code = int(e.response['Error']['Code'])
            if error_code == 403:
                print("Private Bucket. Forbidden Access!")
                return True
            elif error_code == 404:
                self.s3_client.create_bucket(**kwargs)
                return False


    def get_files_with_prefix(self, bucket, prefix):
        """
        Use wildcard search (2020-05-19_*)

        Arguments:
            date_string {[str]} -- [Accept date in string format]
        """

        def resp_to_filelist(resp):
                if 'Contents'  in resp :
                    return [x['Key'] for x in resp['Contents']]
                else :
                    return []

        objects_list = []
        resp = self.s3_client.list_objects(Bucket=bucket, Prefix=prefix)
        objects_list.extend(resp_to_filelist(resp))
        # logger.info("Found {} objects so far".format(len(objects_list)))

        # Only 1000 files are returned in list_objects method,
        # fetch truncated files
        while resp['IsTruncated']:
            last_key = objects_list[-1][0]
            resp = self.s3_client.list_objects(Bucket=bucket,
                                          Prefix=prefix,
                                          Marker=last_key)
            objects_list.extend(resp_to_filelist(resp))
            # logger.info("Found {} objects so far".format(len(objects_list)))

        return objects_list


    def get_date_range(self, sdate, edate):
        """Generate date list for given start_date n end_date

        sdate = date(2008, 8, 15)   # start date
        edate = date(2008, 9, 15)   # end date

        """
        delta = edate - sdate       # as timedelta

        days = [sdate + timedelta(days=i) for i in range(delta.days + 1)]
        return days




    def concat_files(self, result_file_path, file_names) :
        """
        s3 MultipartUploadJob works for file more than 5 mb in size.
        For smaller files need to download and call multipart upload.
        S3Concat handles all such edge cases and provides a combined file.
        """
        job = S3Concat(self.result_bucket, result_file_path, None,
                       content_type='text/plain')
        job.s3 = self.s3_client

        [job.add_files(x.split("/")[1]) for x in file_names]
        job.concat(small_parts_threads=4)

    def move_result_files(self, file_path):
        """
        Move the file  from results bucket to cache bucket.

        :param file_path: Path of the result file
        :return: None
        """
        self.create_bucket_if_required(myconfig.CACHE_BUCKET)
        src = f"{self.result_bucket}/{file_path}"
        dest = f"{self.cache_result_bucket}/{file_path}"
        self.s3_fs.move(src, dest)


    def get_file_url(self, result_file):
        """
        Fetch the url of the file to download
        :param result_file: file path of the result file
        """
        kwargs = {'Bucket': self.cache_result_bucket, 'key': result_file}
        url = self.s3_client.generate_presigned_url(
            ClientMethod='get_object',
            Params={'Bucket': self.cache_result_bucket,
                    'Key': result_file}
        )
        url = url.replace(myconfig.DOCKER_MINIO_HOST, myconfig.LOCAL_MINIO_HOST)
        logger.info("Result url %s" % url)
        return url

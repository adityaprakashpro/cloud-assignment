from celery import Celery
import boto3
import s3fs

from asyncworker.s3Utils import S3Utils
from . import myconfig


# The hardcoded Redis host `redisservice` is a hostname created for the Redis
# container in the Docker network, defined in the docker-compose.yml.
# This is unfortunately specific to the current Docker-compose setup and will
# not work if we want the Redis backend to be located elsewhere.

app = Celery(
    "asyncworker",
    backend=myconfig.REDIS_HOST,
    broker=myconfig.REDIS_HOST
)

# The config can be specified in the myconfig file.
# Please note for minio if its been up using the docker compose
# wil get referred by conainter name


s3_client = boto3.client('s3',
                         endpoint_url='http://' + myconfig.DOCKER_MINIO_HOST,
                         aws_access_key_id=myconfig.AWS_KEY_ID,
                         aws_secret_access_key=myconfig.AWS_SECRETACCESSKEY)

# s3_fs provides a file like api using boto3.
s3_fs = s3fs.S3FileSystem(anon=False, key=myconfig.AWS_KEY_ID,
                          secret=myconfig.AWS_SECRETACCESSKEY,
                          client_kwargs={'endpoint_url': 'http://' + myconfig.DOCKER_MINIO_HOST})


s3_utils = S3Utils(s3_client, s3_fs,
                    myconfig.CACHE_BUCKET,
                    myconfig.RESULT_BUCKET)


app.autodiscover_tasks(["asyncworker.tasks"])

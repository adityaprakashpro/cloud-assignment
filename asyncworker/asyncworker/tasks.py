import heapq
import logging
import time

from asyncworker.celery import app, s3_fs, \
                               s3_client, s3_utils
from datetime import datetime
from celery import chord
from . import myconfig


logger = logging.getLogger(__name__)

bucket_name = myconfig.LOG_BUCKET
result_bucket = myconfig.RESULT_BUCKET
cache_result_bucket = myconfig.CACHE_BUCKET


@app.task(bind=True, name="merge_s3_files")
def merge_s3_files(self, start_date, end_date):
    """
    Async task to download files from S3, merge them and upload the result.
    :return: celery task responsible to merge s3 files.
    """
    self.update_state
    date_format = '%Y-%m-%d'
    start_date = datetime.strptime(start_date, date_format)
    end_date = datetime.strptime(end_date, date_format)
    date_range = s3_utils.get_date_range(start_date, end_date)
    result_file_name =  f"{start_date.strftime(date_format)}#{end_date.strftime(date_format)}+{str(time.time())}"
    # Only make merge_intra_date_logs if log files are present.
    header = [
        merge_intra_date_logs.s(my_date.strftime(date_format))  # using the signature
        for my_date in date_range
        if len(s3_utils.get_files_with_prefix(bucket_name, my_date.strftime(date_format))) != 0
    ]
    callback = merge_inter_day_logs.s(result_file_name = result_file_name)
    result = chord(header)(callback)
    return result



@app.task(bind=True, name="merge_inter_day_logs")
def merge_inter_day_logs(self, rs_files, result_file_name= None):
    """
    Concatenate the list of files into result_file_name.
    :param rs_files: List of files to concat. Each file name
    has date string
     """
    if len(rs_files) == 0:
        return "No log data present."
    rs_files.sort()
    logger.info("Files to merge: %s",  rs_files)
    s3_utils.concat_files(result_file_name, rs_files)
    s3_utils.move_result_files(result_file_name)
    # Delete the rs_files. Intailly the idea was to use
    # the preprocessed data level to get the final result.
    s3_fs.bulk_delete(rs_files)
    return s3_utils.get_file_url(result_file_name)

@app.task(bind=True, name="merge_intra_day_logs")
def merge_intra_date_logs(self, date_string):
    """

    Get all files from s3 for a given date_string
    Example  for '2019-08-17' fetch `2019-08-17_1`, `2019-08-17_2` etc
    Then merge them in sorted manner and write to single file .
    """
    s3_utils.create_bucket_if_required(bucket_name)
    files = [s3_fs.open(f"{bucket_name}/{fn}", "r") for fn
             in s3_utils.get_files_with_prefix(bucket_name, date_string)]
    s3_utils.create_bucket_if_required(result_bucket)
    in_name = date_string + ".log"
    result_file = f"{result_bucket}/{in_name}+{str(time.time())}"
    with s3_fs.open(result_file, 'w') as f:
        f.writelines(heapq.merge(*files))

    return result_file



def download_task_status(task_id):
    '''
    By default clery optimizes unknown task as to pending state.
    No way to differentiate between a real pending task or invalid task.
    Given in our case a valid task is the one with call, handling a
    invalid task at extraction.
    :param task_id:
    :return: task
    '''
    try:
        chord_task = app.AsyncResult(task_id)
        # Fetch the callback of this task
        callback_task_id = chord_task.info[0][0]
        return app.AsyncResult(callback_task_id)
    except TypeError as err:
        return None

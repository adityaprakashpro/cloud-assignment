'''
Properties used across the celery tasks.
In production we can improve this by fetching the values form
the central repository dding the project build. Eg : helm chart or hemidall
'''

AWS_KEY_ID = "minio"
AWS_SECRETACCESSKEY = "minio123"
DOCKER_MINIO_HOST = "minio:9000"
LOCAL_MINIO_HOST = "localhost:9000"
LOCAL_REDIS_HOST = "redis://127.0.0.1:6379"
REDIS_HOST = "redis://redisservice"
LOG_BUCKET = "logs"
RESULT_BUCKET = "results"
CACHE_BUCKET = "cached"